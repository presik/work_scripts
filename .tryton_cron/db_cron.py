#!/bin/bash
import logging
import os

databases = ['PRESIKSAS']

home = os.getenv('HOME')
dir_virtualenvs = os.path.join(home, '.virtualenvs/tryton70/bin/activate')
virtualenvs = f'. {dir_virtualenvs}'

dir_conf = os.path.join(home, '.trytond')
config_file = os.path.join(dir_conf, 'trytond.conf')
for db in databases:
    try:
        command = f'trytond-cron -c {config_file} -d {db}'
        os.system(virtualenvs + ' && sleep 2 && ' + command)
        print('task', db)
    except Exception as e:
        logging.warning('error ...!', e)
