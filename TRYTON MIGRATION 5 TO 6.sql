
DELETE FROM "ir_translation" WHERE "type" = 'error';
ALTER TABLE "ir_translation" DROP CONSTRAINT "ir_translation_translation_md5_uniq";
ALTER TABLE "ir_translation" DROP COLUMN "src_md5";

DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_chorus' AND module = 'account_fr_chorus';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_post_clearing_moves' AND module = 'account_payment_clearing';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_stripe' AND module = 'account_payment_stripe';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_marketing_automation' AND module = 'marketing_automation';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_generate_line_consumption' AND module = 'sale_subscription';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_generate_line_consumption' AND module = 'sale_subscription';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_generate_invoice' AND module = 'sale_subscription';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_role' AND module = 'user_role';
DELETE FROM "ir_model_data" WHERE model = 'res.user' AND "fs_id" = 'user_trigger' AND module = 'res';

UPDATE project_work SET status = db_id FROM ir_model_data WHERE module = 'project' AND fs_id = 'work_open_status' AND state = 'opened';
UPDATE project_work SET status = db_id FROM ir_model_data WHERE module = 'project' and fs_id = 'work_done_status' AND state = 'done';

ALTER TABLE sale_amendment_line DROP CONSTRAINT sale_amendment_line_shipment_party_fkey;

ALTER TABLE ir_model_field ADD COLUMN "access" BOOLEAN default false;
ALTER TABLE ir_model_access ADD COLUMN "active" BOOLEAN default true;
ALTER TABLE res_group ADD COLUMN "active" BOOLEAN default true;

UPDATE account_invoice_line SET currency = (SELECT currency FROM account_invoice WHERE id = account_invoice_line.invoice);
UPDATE account_invoice_line SET invoice_type = (SELECT type FROM account_invoice WHERE id = account_invoice_line.invoice) WHERE invoice_type IS NOT NULL;
UPDATE account_invoice_line SET party = (SELECT party FROM account_invoice WHERE id = account_invoice_line.invoice) WHERE party IS NOT NULL;

DELETE FROM "ir_module" WHERE "name"= 'staff_payroll_access';
DELETE FROM "ir_module" WHERE "name"= 'electronic_mail_template';
DELETE FROM "ir_module" WHERE "name"= 'electronic_mail';
DELETE FROM "ir_module" WHERE "name"= 'smtp';
DELETE FROM "ir_module" WHERE "name"= 'ims';
DELETE FROM "ir_module" WHERE "name"= 'party_reward_points';

UPDATE res_user SET language=(SELECT id FROM ir_lang WHERE name='Spanish (Spain)');
UPDATE ir_lang set translatable=false where code in ('es_419', 'es_CO');
DELETE FROM ir_translation WHERE lang='es_419';
DELETE FROM ir_translation WHERE lang='es_CO';
UPDATE ir_lang set translatable=false where code in ('es_419', 'es_CO');
UPDATE staff_contract set salary=0 WHERE salary is NULL;
UPDATE ir_configuration set language='es';
DELETE FROM ir_translation where name ilike '%lang%' and res_id=9;

CORRER

NOTA: ELIMINAR


DESPUES

UPDATE product_product set description=regexp_replace(description, E'[\\n\\r]+', ' ', 'g' );
ALTER table stock_move DROP COLUMN out_anglo_saxon_quantity;
ALTER table stock_move DROP COLUMN in_anglo_saxon_quantity;

ALTER table ir_sequence_type DROP COLUMN code;
UPDATE account_account_type SET statement='balance' WHERE sequence<30000 and sequence>1;
UPDATE account_account_type SET statement='income' WHERE sequence<80000 and sequence>30000;
UPDATE account_account_type SET revenue='true' WHERE sequence in (44100, 44700, 30100, 45700, 45100);
UPDATE account_account_type SET expense='true' WHERE sequence in (44200, 44300, 44400, 44500, 44600, 30200);
UPDATE account_account_type SET stock='true' WHERE sequence in (11070,
11080,
11090,
11100,
12080,
12090,
12100,
12110,
12120,
12130,
12160
);
UPDATE account_account_type SET payable='true' WHERE sequence in (22101,
22102,
22103,
22104,
22105,
22106,
22107,
22108,
22109,
22110,
22111,
22112,
22113,
22114,
22115,
22201,
22202,
22203,
22204,
22205,
22206,
22207,
22208,
22209,
22210,
22211
);
UPDATE account_account_type SET receivable='true', payable='false' WHERE sequence in (
11030,
11040,
11050,
11060,
11110,
11120,
12030,
12040,
12140);

UPDATE account_account SET party_required=true WHERE code >'37' and type is not null;
UPDATE product_template pt SET code=pp.code FROM product_product pp WHERE pt.id=pp.template;
DELETE FROM ir_ui_view WHERE module='staff_payroll_access';
ALTER TABLE staff_wage_type DROP COLUMN synchronize_access;


Hola esta es la nueva version de Tryton ERP 6.0 a partir de hoy, deben descaragarla e instalarla

https://downloads.tryton.org/lts/tryton-64bit-last.exe

pip3 install tryton==6.0.3

DESPUES DE DESINTALAR STOCK ANGLO Y CONTIN

DELETE FROM "ir_module" WHERE "name"= 'account_stock_anglo_saxon';
DELETE FROM "ir_module" WHERE "name"= 'account_stock_continental';
DELETE FROM  ir_ui_view WHERE module='electronic_mail';

ALTER TABLE stock_move DROP COLUMN in_anglo_saxon_quantity;
ALTER TABLE stock_move DROP COLUMN out_anglo_saxon_quantity;


Migration stock_duration to stock_co

DELETE FROM "ir_module" WHERE "name"= 'stock_duration';
DELETE FROM "ir_model" WHERE "module"= 'stock_duration';
DELETE FROM "ir_model_data" WHERE "module"= 'stock_duration';
DELETE FROM "ir_model_field" WHERE "name"= 'stock_duration';
DELETE FROM "ir_model_field" WHERE "module"= 'stock_duration';
DELETE FROM "ir_module_dependency" WHERE "name"= 'stock_duration';

Corregir codigos de productos duplicados

update product_product SET code=suffix_code where suffix_code != code and suffix_code is not null and suffix_code != '';
update product_product  set suffix_code=null where suffix_code is not null;
