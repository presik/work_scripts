#!/bin/sh

# Backup for databases Postgresql
# Crontab will execute everyday 2:10 a.m.

# Add next line to crontab /etc/crontab
# 10 2   * * *	psk   $HOME/.scripts/backup_db.sh

backup_dir=$HOME/.backups

DBLIST="
	DEMO
"

today="$(date +%Y-%m-%d)"
yesterday="$(date +%Y-%m-%d -d 'yesterday')"
DBLIST=$(psql -t -d postgres -c "SELECT datname FROM pg_database WHERE datallowconn=true AND datname!='postgres' AND datname NOT LIKE '%template%'")


for DB in ${DBLIST}
do
	filename=${DB}-${today}.dump
	/usr/bin/pg_dump --format=c --no-owner -f $backup_dir/$filename $DB
    yesterday_file=$backup_dir/${DB}-${yesterday}.dump
	if [ -e $yesterday_file ]
	    then
			echo "Removing old file... " ${yesterday_file}
	    	rm ${yesterday_file}
	fi
done
