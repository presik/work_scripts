#!/bin/bash



. ~/.virtualenvs/tryton70/bin/activate



# Define your function here
bitClone () {
   echo "Cloning remote Presik repo from Bitbucket... $1"
   sudo rm -fr modules/*
   cd modules
   git clone https://presik@bitbucket.org/presik/trytonpsk-$1.git
   cd trytonpsk-$1
   $HOME/.virtualenvs/tryton70/bin/python3 -m pip  install .
   if [ ! -z $2 ]
   then
       echo "Updating database... $2"
       trytond-admin -v -c ~/.trytond/trytond.conf -u $1 -d $2
   fi
}

# Invoke your function
bitClone "$1" "$2"


