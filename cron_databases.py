import os
import threading
import logging
from logging import config

databases = ["FERMAR", "PRESIKSAS"]
name_env = "tryton60"
daemon_path = os.path.expanduser('~/.virtualenvs/{}/bin/trytond-cron'.format(name_env))

name = "strytond_cron"
desc = "Tryton Cron"
config_path = os.path.expanduser('~/.trytond/trytond.conf')
logging_path = os.path.expanduser('~/.trytond/logger.conf')
options = "-c {}".format(config_path)


def thread_func(database_name):
    db_name = str(database_name).lower()

    log_options = "--logconf {}".format(logging_path)
    pidfile = "/var/run/trytond_cron/{}_{}.pid".format(name, db_name)
    os.system("{} -v {} -d {} --pidfile {} {}".format(daemon_path, options, database_name, pidfile, log_options))


threads = []
for database_name in databases:
    t = threading.Thread(target=thread_func, args=[database_name])
    threads.append(t)

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()
