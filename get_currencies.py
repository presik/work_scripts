import datetime
import os
import sqlite3

import requests


def get_and_store_currencies():
    # Specify the database file path
    my_path = os.path.expanduser('~') + '/currencies'
    db_file = os.path.join(my_path, 'currency.db')

    # Create the database if it doesn't exist
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()

    # Create the table if it doesn't exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS currencies (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            date_currency DATE,
            base TEXT,
            currency TEXT,
            rate REAL
        )
    """)
    # Fetch data from the API
    url = "https://openexchangerates.org/api/latest.json?app_id=02030573b9d74cb18c68de91d87c6e54"
    response = requests.get(url)
    data = response.json()
    # Format the datetime object
    # Insert the data into the database
    for currency, rate in data['rates'].items():
        print(currency, rate)
        cursor.execute("INSERT INTO currencies (date_currency, base, currency, rate) VALUES (?, ?, ?, ?)",
                       (datetime.date.today(), data['base'], currency, rate))

    conn.commit()
    conn.close()


if __name__ == "__main__":
    get_and_store_currencies()
