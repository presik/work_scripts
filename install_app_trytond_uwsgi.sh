#!/bin/bash

#---------------------------------------------------------
#    Script Install Tryton uswgi for Home Intance
# --------------------------------------------------------


#---------------------------------------------------------
#    Fill this variables before run script               #
                                                         #
read -p "domain: " domain                                #
read -p "port: " port                                    #
read -p "user: " user                                    #
#                                                        #
#---------------------------------------------------------


if ! [ -x "$(command -v nginx)" ]; then
  sudo apt update
  sudo apt -y install nginx
  >&2
  echo "nginx is installed"
fi

mkdir /etc/uwsgi
mkdir /etc/uwsgi/vassals
file_location=/etc/uwsgi/vassals/trytond.ini
if [ -f "$file_location" ]; then
  echo "File $file_location already exists!"
else
  cat > $file_location <<EOF
[uwsgi]
master = True # start in master mode
#http = :${port} # bind to port ${port}
socket = /var/run/uwsgi/trytond.sock
workers = 4 # run 4 worker processes
enable-threads = true
# gid
gid             = www-data
# uid
uid             = www-data


virtualenv = /home/${user}/.virtualenvs/tryton70
env = TRYTOND_CONFIG=/home/${user}/.trytond/trytond.conf
module = trytond.application:app
chmod-socket = 777
procname = TrytodServer
vacuum = true
die-on-term = true

single-interpreter = true
die-on-term = true                   ; Shutdown when receiving SIGTERM (default is respawn)
need-app = true

disable-logging = true               ; Disable built-in logging
log-4xx = true                       ; but log 4xx's anyway
log-5xx = true                       ; and 5xx's

harakiri = 900                        ; forcefully kill workers after 60 seconds
py-callos-afterfork = true           ; allow workers to trap signals

max-requests = 1000                  ; Restart workers after this many requests
max-worker-lifetime = 3600           ; Restart workers after this many seconds
reload-on-rss = 2048                 ; Restart workers after this much resident memory
worker-reload-mercy = 60             ; How long to wait before forcefully killing workers

cheaper-algo = busyness
processes = 128                      ; Maximum number of workers allowed
cheaper = 8                          ; Minimum number of workers allowed
cheaper-initial = 16                 ; Workers created at startup
cheaper-overload = 1                 ; Length of a cycle in seconds
cheaper-step = 16                    ; How many workers to spawn at a time

cheaper-busyness-multiplier = 30     ; How many cycles to wait before killing workers
cheaper-busyness-min = 20            ; Below this threshold, kill workers (if stable for multiplier cycles)
cheaper-busyness-max = 70            ; Above this threshold, spawn new workers
cheaper-busyness-backlog-alert = 16  ; Spawn emergency workers if more than this many requests are waiting in the queue
cheaper-busyness-backlog-step = 2    ; How many emergency workers to create if there are too many requests in the queue

EOF
fi

file_location2=/etc/systemd/system/trytond.service
if  [ -f "$file_location2" ]; then
  echo "File $file_location2 already exists!"
else
  cat > $file_location2 <<EOF
# Script Tryton Server Presik Technologies

[Unit]
Description=uWSGI Trytond Server
After=syslog.target

[Service]
User=${user}
ExecStart=/home/${user}/.virtualenvs/tryton70/bin/uwsgi --ini /etc/uwsgi/vassals/trytond.ini
# Requires systemd version 211 or newer
RuntimeDirectory=uwsgi
#Restart=always
KillSignal=SIGQUIT
Type=notify
StandardError=syslog
NotifyAccess=all

[Install]
WantedBy=multi-user.target

EOF
fi


file_location3=/etc/nginx/sites-available/trytond
if [ -f "$file_location3" ]; then
  echo "File $file_location3 already exists!"
else
  cat > $file_location3 <<EOF
upstream trytond {
    server unix:/var/run/uwsgi/trytond.sock; # for a file socket
}

# configuration of the server
server {
    # the port your site will be served on
    listen      ${port};
    # the domain name it will serve for
    server_name ${domain}; # substitute your machine's IP address or FQDN

    access_log /var/log/nginx/${domain}_access.log;
    error_log /var/log/nginx/${domain}_error.log;

    # max upload size
    client_max_body_size 2048M;   # adjust to taste

    #location /static {
    #    alias /<path_to>/static; # your Django project's static files - amend as required
    #}
    
    error_page 497 301 =307 https://${domain}:${port}$request_uri;

    # Finally, send all non-media requests.
    location / {
        add_header 'Access-Control-Allow-Origin' '*';
        include     uwsgi_params; # the uwsgi_params file you installed
        uwsgi_param REMOTE_USER '$remote_user';
        uwsgi_param DATE_GMT '$date_gmt';
        uwsgi_param DATE_LOCAL '$date_local';
        uwsgi_param AUTH_TYPE Basic;
        uwsgi_read_timeout 600s;
        uwsgi_pass  trytond;
    }

    location /status_server {
       stub_status on;
       allow 127.0.0.1;
       deny all;
    }
   
    listen              [::]:${port} ssl ipv6only=on; # managed by Certbot
    listen              ${port} ssl; # managed by Certbot
    ssl_certificate     /etc/letsencrypt/live/${domain}/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/${domain}/privkey.pem; # managed by Certbot
    include             /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam         /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
    #ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    #ssl_ciphers         HIGH:!aNULL:!MD5;

}

  
EOF
fi

ln -s /etc/nginx/sites-available/trytond /etc/nginx/sites-enabled/

ufw allow 'Nginx Full'
ufw allow 8000
ufw reload
systemctl enable trytond.service
systemctl start trytond.service
systemctl stop nginx
systemctl start nginx
