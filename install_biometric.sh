#!/bin/bash


#---------------------------------------------------------
#    Fill this variables before run script               #
read -p "ingresa el dominio donde se enuentra la base de datos tryton ej: cloudx.presik.com o localhost: " domain                                                        #
read -p "port api tryton ej 8010: " port_tryton  
read -p "port api biometric ej 8010: " port_biometric 
read -p "NODE ENV options 'development' for http or 'production' for https: " env                                              #
read -p "Nombre de base de datos: " db_name                                          #
#                                                        #
#---------------------------------------------------------

cd
if ! [ -x "$(command -v curl)" ]; then
  sudo apt -y install curl
  >&2
  echo "curl is installed"
fi

if ! [ -x "$(command -v node)" ]; then
  curl -sL https://deb.nodesource.com/setup_18.x | sudo bash -
  sudo apt -y install nodejs
  >&2
  echo "node is installed"
fi

cd $HOME
mkdir biometric
cd biometric
git clone https://developer_presik@bitbucket.org/presik/app-bio.git
git clone https://developer_presik@bitbucket.org/presik/api-bio.git
sudo apt-get install libcairo2 libcairo2-dev libgirepository1.0.dev gir1.2-fprint-2.0 python3-dev

python3 -m venv venv
cd api-bio
$HOME/biometric/venv/bin/pip3 install -r requirements.txt

cd ..
cd app-bio


cat >.env <<EOF

REACT_APP_BIOMETRIC_API_HOST="127.0.0.1"
REACT_APP_BIOMETRIC_API_PORT="$port_biometric"
REACT_APP_TRYTON_API_HOST="$domain"
REACT_APP_TRYTON_API_PORT="$port_tryton"
REACT_APP_DB="$db_name"
DISABLE_ESLINT_PLUGIN=true
REACT_APP_ENV="$env"
EOF

npm install
cd ..


file_location=/etc/udev/rules.d/50-usb.rules
if  [ -f "$file_location" ]; then
  echo "File $file_location already exists!"
else
  cat <<EOF | sudo tee -a ${file_location}
SUBSYSTEM=="usb", ATTRS{idVendor}=="05ba", ATTRS{idProduct}=="000a", GROUP="users", MODE="0666"
EOF
fi

file_location2=/etc/systemd/system/api_bio.service
if  [ -f "$file_location2" ]; then
  echo "File $file_location2 already exists!"
else
  cat <<EOF | sudo tee -a ${file_location2}
# Script biometric Technologies

[Unit]
Description= Server Biometric
After=network.target

[Service]
User=$USERNAME
WorkingDirectory=$HOME/biometric/api-bio
ExecStart=$HOME/biometric/venv/bin/python3 run.py
#ExecStop=

[Install]
WantedBy=multi-user.target

# Protect clock, allow USB and SPI device access
ProtectClock=yes
DeviceAllow=char-usb_device rw
DeviceAllow=char-spi rw
DeviceAllow=char-hidraw rw

# Allow tuning USB parameters (wakeup and persist)
ReadWritePaths=/sys/devices


EOF
fi

file_location3=/etc/systemd/system/app_bio.service
if  [ -f "$file_location3" ]; then
  echo "File $file_location3 already exists!"
else
  cat <<EOF | sudo tee -a ${file_location3}
# Script Server Presik Technologies

[Unit]
Description=App Biometric Web

[Service]
User=$USERNAME
Group=$USERNAME
WorkingDirectory=$HOME/biometric/app-bio
ExecStart=npm run start
ExecStop=

[Install]
WantedBy=multi-user.target

EOF
fi

sudo systemctl daemon-reload

sudo systemctl enable api_bio.service
sudo systemctl stop api_bio.service
sudo systemctl start api_bio.service
sudo systemctl status api_bio.service

sudo systemctl enable app_bio.service
sudo systemctl stop app_bio.service
sudo systemctl start app_bio.service
sudo systemctl status app_bio.service
