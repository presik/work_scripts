#!/bin/sh

VENV="apiface"
DIR_PROJECT="$HOME/api-face"

echo "-----------------------------------------------------------------------------------"
echo "                          INSTALLING API FACE RECOGNITION                          "
echo "-----------------------------------------------------------------------------------"
echo
cd $HOME
if [ -d "api-face" ]; then
    echo "The directory 'api-face' already exists. Skipping git clone."
else
    git clone https://presik@bitbucket.org/presik/api-face.git
fi
echo
echo "Creating virtualenv $VENV ... "
echo
python3 -m venv $HOME/.virtualenvs/${VENV}

cd $DIR_PROJECT

mkdir -p "$HOME/.apiface"
file_location="$HOME/.apiface/api-face.ini"

if [ -f "$file_location" ]; then
    echo "File $file_location already exists!"
else
    echo "Creating api configuration..."
    echo
    read -p "Enter the host where the api will run e.g. 0.0.0.0: " host
    read -p "Enter the port where the api will run e.g 8010: " port
    read -p "Do you want to associate any SSL certificate to the api [y/n]: " ssl
    echo
    if [ "$ssl" = "y" ]; then
        read -p "Enter the certificate path e.g. /etc/ssl/api.crt: " ssl_cert
        read -p "Enter the path to the private certificate e.g. /etc/ssl/api.key: " ssl_key
    fi

    cat <<EOF > "$file_location"
[General]
host = $host
port = $port

[Auth]
api_tokens = []
EOF
    if [ "$ssl" = "y" ]; then
        cat <<EOF >> "$file_location"
[SSL]
cert_file = $ssl_cert
key_file = $ssl_key
EOF
    fi
    cat <<EOF >> "$file_location"
[CORS]
origins = ["http://localhost", "http://localhost:3000"]
EOF
    sudo chmod 644 "$file_location"
fi

. ~/.virtualenvs/$VENV/bin/activate

python3 -m pip install -r requirements.txt

file_service_location="/etc/systemd/system/api-face.service"

if [ -f "$file_service_location" ]; then
    echo "File $file_service_location already exists!"
else
    echo
    echo "Creating systemd service..."
    echo
    cat <<EOF | sudo tee "$file_service_location"
# Script Server Presik API Technologies

[Unit]
Description=Api Face Recognition Server

[Service]
User=$(whoami)
WorkingDirectory=$DIR_PROJECT
Environment="PATH=$HOME/.virtualenvs/$VENV/bin:/usr/bin"
Environment="CONFIG_FILE=api-face.ini"
ExecStart=$HOME/.virtualenvs/$VENV/bin/python3 run.py
Restart=always
RestartSec=5
StartLimitInterval=400
StartLimitBurst=3

[Install]
WantedBy=multi-user.target
EOF
fi

echo
echo "--------------------------------------------------------------------------------------------------"
echo "¡REMEMBER TO CONFIGURE THE LIST OF AUTHORIZED TOKENS IN THE AUTH SECTION OF THE API CONFIGURATION!"
echo "--------------------------------------------------------------------------------------------------"
echo

sudo systemctl daemon-reload
sudo systemctl enable api-face.service
sudo systemctl restart api-face.service
sudo systemctl status api-face.service
