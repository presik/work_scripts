git clone https://presik@bitbucket.org/presik/kid.git
cp kid /home/psk
read -p "domain:" domain
read -p "API_KEY:" apikey
read -p "environment: (production, development)" env

file_location=/home/psk/kid/.env
if [ -f "$file_location" ]; then
    echo "File $file_location already exists!"
else
    cat > $file_location <<EOF

REACT_APP_TRYTON_API_HOST="$domain"
REACT_APP_TRYTON_API_PORT="8010"
REACT_APP_TRYTON_API_KEY="$apikey"
REACT_APP_ENV="$env"
DISABLE_ESLINT_PLUGIN=true

EOF
fi
sudo mkdir /var/www/html/kid/
cd /home/psk/kid
yarn install
yarn build
cd ..
sudo cp -r /home/psk/kid/build/* /var/www/html/kid/
sudo systemctl stop nginx
sudo systemctl start nginx
sudo systemctl status nginx