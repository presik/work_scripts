#!/bin/sh

#curl -L -o neupos.tgz -H "User-Agent: Mozilla/5.0" "https://bitbucket.org/presik/work_scripts/downloads/neupos.tgz"
install_dir="$HOME/.neupos"

if [ -d "$install_dir" ]; then
    echo "Eliminando directorio existente: $install_dir"
    rm -rf "$install_dir"
fi
mkdir -p "$install_dir"

tar -xzvf neupos.tgz -C $HOME/.neupos

echo "[INFO] Creating POS launcher..."


desktop_file="$(xdg-user-dir DESKTOP)/neupos.desktop"

if [ -f "$desktop_file" ]; then
    rm "$desktop_file"
fi

read -p "Paquete a instalar (appimage/deb/rpm): " app  

echo $app 

if [[ "$app" == "appimage" ]]; then
    # dependencies
    sudo apt-get install -y libfuse2t64 
    exec_cmd="env DATABASE_URL=\"sqlite:///var/lib/neupos/presik_pos.db?mode=rwc\" \"$(ls $HOME/.neupos/appimage/*.AppImage | head -n 1)\""
elif [[ "$app" == "deb" ]]; then
    sudo apt install -y $(ls $HOME/.neupos/deb/*.deb | head -n 1)
    exec_cmd="env DATABASE_URL=\"sqlite:///var/lib/neupos/presik_pos.db?mode=rwc\" neu-pos"
elif [[ "$app" == "rpm" ]]; then
    sudo yum install -y $(ls $HOME/.neupos/rpm/*.rpm | head -n 1)
    exec_cmd="env DATABASE_URL=\"sqlite:///var/lib/neupos/presik_pos.db?mode=rwc\" neu-pos"
fi

data_launcher="[Desktop Entry]
Version=$version
Name=Neu POS
Comment=POS Client Tryton
Exec=$exec_cmd
Icon=$HOME/.neupos/icons/Square142x142Logo.png
Terminal=false
Type=Application
Categories=Utility;Application;"

desktop=$(xdg-user-dir DESKTOP)
desktop_file="$desktop/neupos.desktop"
echo "$data_launcher" > "$desktop_file"

# Dar permisos de ejecución al .desktop para que pueda ejecutarse desde el menú
chmod +x "$desktop/neupos.desktop"
gio set "$desktop_file" "metadata::trusted" true

echo "[INFO] Adding user target to lpadmin... "
sudo usermod -a -G lp $USER
sudo usermod -a -G lpadmin $USER
sudo usermod -a -G dialout $USER
echo "[INFO] Done."


dir_db=/var/lib/neupos/
sudo mkdir -p $dir_db
sudo chown -R $USER:$USER $dir_db
sudo chmod 755 -R $dir_db
