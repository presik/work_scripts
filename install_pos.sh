#!/bin/sh

#---------------------------------------------------------
#    Script Install Tryton POS Client
# --------------------------------------------------------

# Main functions/variables declaration
version='6.0'
dir_pos_app=$HOME/.pos_app


#Create directories for tryton environment
message "[INFO] Creating tryton target directories... " ${BLUE}
dir_config_tryton=$HOME/.tryton
mkdir $dir_config_tryton
chmod 755 -R ${dir_config_tryton}
chown $USER:$USER -R ${dir_config_tryton}

mkdir $dir_pos_app


# Install sudo apt packages

echo "[INFO] Installing main sudo apt packages..."
#sudo sudo apt update
#sudo sudo apt -y upgrade
sudo apt -y install python-setuptools
sudo apt -y install libusb-1.0-0
sudo apt -y install libusb-1.0-0-dev
sudo apt -y install libjpeg8 libjpeg62-dev libfreetype6 libfreetype6-dev
sudo apt -y install libssl-dev libffi-dev
sudo apt -y install python3-pip
sudo apt -y install python3-PySide6
sudo apt -y install python3-dateutil
sudo apt -y install python3-pillow
sudo apt -y install python3-serial
sudo apt -y install python3-gi
sudo apt -y install python3-gi-cairo

echo "[INFO] Done."

# Add install sale pos module

echo "[INFO] Installing pip packages..."

pip3 install pyusb
pip3 install pillow
pip3 install qrcode
pip3 install paramiko
pip3 install pyserial
pip3 install pyudev
pip3 install orjson
pip3 install escpos
pip3 install packaging
pip3 install PySide6>=6.4.1


echo "[INFO] Done."

echo "[INFO] Installing client POS..."
git clone https://bitbucket.org/presik/presik_pos


mv presik_pos $dir_pos_app
chown -R $USER:$USER $dir_pos_app

pos_client_dir=$dir_pos_app/presik_pos

cd $pos_client_dir

cp $pos_client_dir/config_pos.ini $dir_config_tryton
chown -R $USER:$USER $dir_config_tryton
chmod 755 -R $dir_config_tryton


echo "[INFO] Done."


echo "[INFO] Creating POS launcher..."

data_launcher="
[Desktop Entry]\n
Version=$version\n
Name=Caja POS\n
Comment=POS Client Tryton\n
Exec=python3 $pos_client_dir/pospro\n
Icon=$pos_client_dir/app/share/pos-icon.svg\n
Terminal=false\n
Type=Application\n
Categories=Utility;Application;\n
"

echo $data_launcher >> $HOME/Escritorio/cajapos.desktop

sudo chown $USER:$USER $HOME/Escritorio/cajapos.desktop
echo "[INFO] Done."

message "[INFO] Adding user target to lpadmin... " ${BLUE}
sudo usermod -a -G lp $USER
sudo usermod -a -G lpadmin $USER
sudo usermod -a -G dialout $USER
echo "[INFO] Done."

echo "[INFO] On Xubuntu removing apps no required..."
sudo apt -y purge parole gmusicbrowser pidgin gnome-sudoku gnome-mines thunderbird
sudo apt -y autoremove
echo "[INFO] Done."
