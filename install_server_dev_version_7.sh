#!/bin/sh

#---------------------------------------------------------
#    Script Install Tryton Server
# --------------------------------------------------------

# Note: run this script with as user (not root), so doesn't use sudo for run it
# Main functions/variables declaration

# Colors constants
NONE="$(tput sgr0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="\n$(tput setaf 3)"
BLUE="\n$(tput setaf 4)"

message () {
    # $1 : Message
    # $2 : Color
    # return : Message colorized
    local NOW="[$(date +%H:%M:%S)] "

    echo "${2}${NOW}${1}${NONE}"
}


echo '----------------------------------------------------------'
#read -p 'Introduce the tryton name user: ' nameuser
version='7.0'
max_version='7.1'

integer_version='70'
venv='tryton'${integer_version}
nameuser=$SUDO_USER
# HOME_USER=/home/psk
HOME_USER=$HOME
file_bashrc=$HOME_USER'/.bashrc'

PYT_CMD=$HOME_USER/.virtualenvs/${venv}/bin/python3

# apt get update

# Install PIP packages
PIP_CMD=$HOME_USER/.virtualenvs/${venv}/bin/pip3

source_="\nalias workon='source ~/.virtualenvs/$venv/bin/activate'"
export_="\nexport WORKON_HOME=$HOME_USER/.virtualenvs"

message "[INFO] Creating Predash directory and files..." ${BLUE}
mkdir $HOME_USER/.fastapi
mkdir $HOME_USER/.certificate

git clone https://presik@bitbucket.org/presik/api-fast.git
git clone https://presik@bitbucket.org/presik/kid.git

cp api-fast/api-fast.ini ~/.fastapi/api-fast.ini
sudo cp api-fast/api-fast.service /etc/systemd/system/api-fast.service
sudo chmod 755 /etc/systemd/system/api-fast.service
message "[INFO] Done." ${YELLOW}

# Create directories for tryton environment
message "[INFO] Creating tryton target directories... " ${BLUE}
CONFIG_DIR="$HOME_USER/.trytond"
ATTACH_DIR="$HOME_USER/.attach"
BACKUP_DIR="$HOME_USER/.backups"
SCRIPTS_DIR="$HOME_USER/.scripts"

mkdir ${CONFIG_DIR}
chown -R ${nameuser}:${nameuser} ${CONFIG_DIR}

mkdir ${ATTACH_DIR}
chmod 755 ${ATTACH_DIR}

mkdir ${BACKUP_DIR}
chmod 755 ${BACKUP_DIR}

mkdir ${SCRIPTS_DIR}
chmod 755 ${SCRIPTS_DIR}

cp backup_db.sh $SCRIPTS_DIR
cp vacuum_db.sh $SCRIPTS_DIR

cp trytond.conf ${CONFIG_DIR}/trytond.conf
chmod 755 ${CONFIG_DIR}/trytond.conf

message "[INFO] Done." ${YELLOW}
message "[INFO] Done." ${YELLOW}

# Install apt-get packages
message "[INFO] Installing main apt-get packages..." ${BLUE}

appackages="
  git
  python3-full
  python3-dev
  plocate
  htop
  gcc
  postgresql
  postgresql-contrib
  libreoffice
"
python3 -m venv $HOME_USER/.virtualenvs/${venv}

for i in ${appackages}
    do
        sudo apt -y install $i
    done
message "[INFO] Done." ${YELLOW}

#  Create Virtualenv
message "[INFO] Creating virtualenv... $venv " ${BLUE}

echo $source_ >> $file_bashrc
echo $export_ >> $file_bashrc

# Install PIP package
message "[INFO] Installing main PIP packages..." ${BLUE}

. ~/.virtualenvs/$venv/bin/activate

pippackages="
    uvicorn
    fastapi
    uwsgi
    polars
    pandas
    psycopg2-binary
    psycopg2
    jinja2
    python-stdnum
    qrcode
    xmltodict
    image
    orjson
    pycountry
    gunicorn
    openpay
    sendgrid
    bcrypt
    psk_numword
"

for i in ${pippackages}
    do
        ${PIP_CMD} install $i
    done

tryton_modules="
    trytond
    trytond_company
    trytond_currency
    trytond_party
    trytond_country
    trytond_account
    trytond_account_invoice_history
    trytond_account_invoice_stock
    trytond_account_payment
    trytond_account_budget
    trytond_account_asset
    trytond_account_statement
    trytond_product_attribute
    trytond_product_cost_history
    trytond_account_product
    trytond_account_invoice
    trytond_account_credit_limit
    trytond_purchase
    trytond_purchase_requisition
    trytond_purchase_shipment_cost
    trytond_product
    trytond_product_price_list
    trytond_product_price_list_dates
    trytond_stock
    trytond_stock_supply_day
    trytond_stock_forecast
    trytond_stock_inventory_location
    trytond_stock_product_location
    trytond_stock_location_sequence
    trytond_stock_supply_forecast
    trytond_stock_lot
    trytond_stock_supply
    trytond_stock_split
    trytond_stock_lot_sled
    trytond_sale
    trytond_sale_stock_quantity
    trytond_sale_credit_limit
    trytond_sale_discount
    trytond_sale_price_list
    trytond_sale_supply
    trytond_production
    trytond_production_routing
    trytond_production_work
    trytond_bank
    trytond_project
    trytond_timesheet
    trytond_commission
    trytond_analytic_account
    trytond_company_work_time
    trytond_purchase_request
    trytond_analytic_purchase
    trytond_analytic_sale
    trytond_analytic_invoice
    trytond_sale_invoice_grouping
    proteus
"

message "[INFO] Installing official Tryton packages..." ${BLUE}

for i in ${tryton_modules}
do
    ${PYT_CMD} -m pip install "$i>=$version,<$max_version"
done
message "[INFO] Done. " ${YELLOW}

#install tryton presik modules

tryton_psk_modules="account_bank_statement
	party_personal
	account_co_pyme
	account_exo
	account_col
	account_statement_co
	account_voucher
	account_budget_co
	account_stock_latin
	account_invoice_discount
	electronic_invoice_co
	invoice_report
	collection
	sale_co
	sale_pos
	sale_shop
	sale_pos_frontend
	sale_pos_frontend_rest
	sale_contract
	sale_salesman
	stock_co
	commission_co
	purchase_co
	purchase_suggested
	purchase_editable_line
	product_asset_attribute
	product_onebarcode
	product_image
	project_co
	invoice_project
	production_accounting
	company_operation
	payment_openpay
	company_operation_shop
	hotel
	hotel_wubook
    maintenance
	company_department
	goal
	dash
	dash_sale
	dash_stock
	dash_purchase
	email
	smtp
	staff
	staff_co
	staff_event
	staff_payroll
	staff_access
	staff_project
	staff_payroll_project
	staff_access_extratime
	staff_contracting
	staff_loan
	staff_payroll_co
	staff_social_security
	electronic_payroll
	crm
	log
	analytic_co
	analytic_sale_contract
	analytic_payroll
	analytic_sale_pos
"

mkdir $HOME_USER/modules
cd  ${HOME_USER}/modules

# . ~/.virtualenvs/$venv/bin/activate

for i in ${tryton_psk_modules}
do
    git clone https://bitbucket.org/presik/trytonpsk-$i.git
	cd trytonpsk-$i
	pip install .
	cd ..
done

# Copying initializing install server script
message "[INFO] Setting initializing trytond server... " ${BLUE}
DAEMON=$HOME_USER/.virtualenvs/${venv}/bin/trytond
SETTING='-c $HOME_USER/.trytond/trytond.conf'
init_cmd="su ${nameuser} -c '${DAEMON} ${SETTING}'"

# Changing owner of new virtualenv
message "[INFO] Changing owner of virtualenv... " ${BLUE}
chown $nameuser:$nameuser -R $HOME_USER/.virtualenvs/
message "[INFO] Done." ${YELLOW}

#Change permissions of /var/lib/trytond
message "[INFO] Changing permissions of /var/lib/trytond... " ${BLUE}
sudo mkdir /var/lib/trytond
sudo chown ${nameuser}:${nameuser} /var/lib/trytond
sudo chmod -R 755 /var/lib/trytond


# Initializing server

# sudo cp strytond.service /etc/systemd/system/strytond.service
# sudo chmod 755 /etc/systemd/system/strytond.service

# sudo cp strytond_service /usr/local/bin/strytond_service
# sudo chmod 755 /usr/local/bin/strytond_service

# sudo systemctl enable strytond.service
# message "[INFO] Done." ${YELLOW}

# message "[INFO] Starting trytond server... " ${BLUE}
# sudo systemctl start strytond.service
# message "[INFO] Ok." ${YELLOW}

# sudo systemctl status strytond.service

message "[INFO] Adding script backup and maintenance of database... " ${BLUE}
sed -i "s/DEMO/${new_db}/g" backup_db.sh
cp backup_db.sh $HOME_USER/.scripts/backup_db.sh
cp renew_certificate.sh $HOME_USER/.scripts/renew_certificate.sh
chmod 755 $HOME_USER/.scripts/backup_db.sh
echo "10 8   * * *	$SUDO_USER    $HOME_USER/.scripts/backup_db.sh" > /etc/crontab
echo "0  3    * * *   root    sh /home/psk/.scripts/renew_certificate.sh" > /etc/crontab
echo "[INFO] Done."


echo
message "[INFO] Your Tryton Server is ready...!" ${YELLOW}

# Add currencies to database
# trytond_import_currencies -c ~/.trytond/trytond.conf -d DEMO60
# trytond_import_countries -c ~/.trytond/trytond.conf -d DEMO60
# trytond_import_postalcodes -c ~/.trytond/trytond.conf -d DEMO60 CO

source .bashrc
