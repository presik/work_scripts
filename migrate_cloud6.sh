#!/bin/sh

#---------------------------------------------------------
#    Script Install Tryton for Home Intance
# --------------------------------------------------------

# Main functions/variables declaration

# Colors constants
NONE="$(tput sgr0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="\n$(tput setaf 3)"
BLUE="\n$(tput setaf 4)"

message () {
    # $1 : Message
    # $2 : Color
    # return : Message colorized
    local NOW="[$(date +%H:%M:%S)] "

    echo "${2}${NOW}${1}${NONE}"
}


echo '----------------------------------------------------------'
#read -p 'Introduce the tryton name user: ' nameuser
version='6.0'
max_version='6.1'
integer_version='60'
venv='tryton60'
nameuser='psk'

# Install new apt packages

sudo apt install python3-gi-cairo

# Create Virtualenv
message "[INFO] Creating virtualenv... $venv " ${BLUE}

python3 -m venv ${HOME}/.virtualenvs/${venv}

source_="\nalias workon='source ~/.virtualenvs/$venv/bin/activate'"
message "Se realiza source"


message "[INFO] Done." ${YELLOW}
read -p "Was created virtualenv $venv? " response

message "[INFO] Your Virtualenv $venv is ready...!" ${YELLOW}
PYT_CMD=${HOME}/.virtualenvs/${venv}/bin/python3

source ~/.virtualenvs/${venv}/bin/activate

# Install PIP packages
PIP_CMD=${HOME}/.virtualenvs/${venv}/bin/pip3

message "[INFO] Installing main PIP3 packages..." ${BLUE}
pippackages="
    wheel
    psycopg2
    psycopg2-binary
    bcrypt
    pytz
    wrapt
    qrcode
    cryptography
    pyOpenssl
    polib
    python-stdnum
    jinja2
    python-sql
    zeep
    psk_numword
    vobject
    simpleeval
    cached-property
    relatorio
    chardet
    passlib
    requests
    python-magic
    flask==1.1.4
    flask_tryton
    flask_cors
    gunicorn
    Werkzeug==1.0.1
    sendgrid
    simplejson
    pycrypto
    pycountry
    python-barcode
    xmltodict
    markupsafe==2.0.1
"
for i in ${pippackages}
    do
        ${PIP_CMD} install $i
    done


tryton_modules="
  country
  party
  currency
  company
  product
  stock
  account
  account_product
  account_invoice
  account_invoice_history
  account_statement
  account_invoice_stock
  account_asset
  bank
  account_payment
  product_cost_fifo
  product_cost_history
  product_price_list
  product_attribute
  stock_forecast
  stock_inventory_location
  stock_product_location
  stock_location_sequence
  stock_product_location
  stock_split
  stock_lot
  stock_lot_sled
  purchase
  purchase_request
  purchase_requisition
  purchase_shipment_cost
  production
  stock_supply
  stock_supply_day
  stock_supply_forecast
  sale
  sale_supply
  sale_opportunity
  sale_price_list
  sale_invoice_grouping
  sale_credit_limit
  analytic_account
  analytic_invoice
  analytic_purchase
  analytic_sale
  account_credit_limit
  commission
  timesheet
  company_work_time
  project
  account_payment
  production_routing
  production_task
  production_work
  account_stock_anglo_saxon
  account_continental
"

message "[INFO] Installing official Tryton packages..." ${BLUE}
for i in ${tryton_modules}
do
    ${PIP_CMD} install "trytond_$i>=6.0.0,<6.2.0"
done
message "[INFO] Done. " ${YELLOW}


message "[INFO] Installing psk Tryton packages..." ${BLUE}
modules="
  account_co_pyme
  account_col
  account_exo
  electronic_invoice_co
  account_voucher
  account_bank_statement
  party_personal
  company_department
  reports
  staff
  staff_co
  staff_event
  staff_payroll
  staff_payroll_co
  staff_contracting
  staff_access
  staff_access_extratime
  sale_goal
  invoice_report
  account_invoice_update
  company_timezone
  company_location
  stock_duration
  account_invoice_discount
  product_onebarcode
  product_reference
  product_image
  purchase_discount
  purchase_editable_line
  sale_discount
  sale_shop
  sale_salesman
  sale_pos
  sale_contract
  sale_pos_frontend
  purchase_report
  purchase_suggested
  account_invoice_discount
  surveillance
  email
  crm
  dash
  dash_sale
  dash_stock
  sale_web_channel
  sale_cost
  stock_product_cost
  product_asset_attribute
  account_invoice_discount_wizard
  stock_shipment_barcode
  invoice_editable_line
  account_stock_latin
  analytic_report
  analytic_voucher
  analytic_payroll
  staff_project
  staff_payroll_project
  stock_co
  purchase_co
  collection
  sale_pos_frontend_rest
  maintenance
  maintenance_reading
  notification_document
  electronic_payroll
"

for i in ${modules}
    do
        git clone https://presik@bitbucket.org/presik/trytonpsk-$i.git
        cd trytonpsk-$i
        ${PYT_CMD} setup.py install
        cd ..
    done
message "[INFO] Done. " ${YELLOW}


# Changing owner of new virtualenv
message "[INFO] Changing owner of virtualenv... " ${BLUE}
chown $nameuser:$nameuser -R ${HOME}/.virtualenvs/
message "[INFO] Done." ${YELLOW}

#systemctl stop strytond.service
#systemctl disable strytond.service

# Enable:
#systemctl enable strytond.service

# Reboot
