delete from ir_module where name='analytic_voucher';
delete from ir_module where name='analytic_report';
delete from ir_module where name='account_invoice_update';
delete from ir_module where name='reports';
delete from ir_module where name='stock_duration';
delete from ir_module where name='stock_product_cost';
delete from ir_module where name='stock_shipmet_barcode';
delete from ir_module where name='syncronize_companies';
delete from ir_module where name='sale_goal';
delete from ir_module where name='carrier';
delete from ir_module where name='sale_opportunity';
delete from ir_module where name='company_location';
delete from ir_module where name='commission_global';
delete from ir_module where name='purchase_report';
delete from ir_module where name='product_reference';
delete from ir_module where name='company_timezone';
delete from ir_module where name='party_card';
delete from ir_module where name='notification_document';   
delete from ir_module where name='crm';
delete from ir_module where name='salesman';
delete from ir_module where name='account_stock_continental';
 
delete from ir_ui_menu where id in (SELECT db_id FROM ir_model_data where module='company_location' and model='ir.ui.menu');
delete from ir_ui_menu where id in (SELECT db_id FROM ir_model_data where module='sale_goal' and model='ir.ui.menu');


update ir_sequence set company=null where name='Default Account Journal';
alter table ir_sequence_type drop column code; 

alter table crm_opportunity rename column city to city_old;
alter table laboratory_order_line rename column code to code_tharsis;


alter table vps_document_electronic drop column period;

---- post_migration ----

UPDATE crm_opportunity AS cr SET city=a.city_id
    FROM (
        SELECT
        ci.id AS city_id,
        ci.name AS name_a,
        cs.dian_code || ci.dian_code AS xcode
        FROM country_city AS ci
        INNER JOIN country_subdivision AS cs ON cs.id=ci.subdivision
    ) a
    INNER JOIN (
        SELECT
        pcc.id AS city_id,
        pcc.name AS name_b,
        pdc.code || pcc.code AS xcode
        FROM party_city_code AS pcc
        INNER JOIN party_department_code AS pdc ON pdc.id=pcc.department
    ) b ON a.xcode=b.xcode
    WHERE city_old=b.city_id AND city IS NULL;
alter table crm_opportunity drop column city_old;


UPDATE sale_contract 
SET agent = mytable.agent
FROM (
    SELECT 
        commission_agent.id as agent, 
        company_employee.id as salesman
    FROM 
        company_employee
    JOIN 
        commission_agent ON company_employee.party = commission_agent.party
) AS mytable
WHERE 
    sale_contract.agent IS NULL 
    AND sale_contract.salesman IS NOT NULL
    AND sale_contract.salesman = mytable.salesman;
alter table sale_contract drop column salesman;

# TODOREPUESTOS
delete from ir_action where id=362;
