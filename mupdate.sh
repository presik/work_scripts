#!/bin/sh

#--------------------------------------------------------------
#  Script Automate Update Tryton Module
# -------------------------------------------------------------


nameuser='psk'
homex='/home/'${nameuser}
venv='tryton50'
dir_source=$homex'/source/'
databases=$(psql -d postgres -c "SELECT datname FROM pg_database WHERE datallowconn='true' AND datname != 'postgres' AND datname NOT LIKE '%template%'")

modules="$@"

echo Number of arguments: $#
echo Arguments: ${modules}
i=0
souhome() {
        workon $venv
        echo Removing all under: ${dir_source}
        sudo rm -R ${dir_source}*
        for module in ${modules}
                do
                        git clone https://presik@bitbucket.org/presik/trytonpsk-${module}.git
                        mv trytonpsk-${module} ${dir_source}
                        cd ${dir_source}/trytonpsk-${module}
                        python setup.py install
                done
                for DB in ${databases}
                        do
                                echo "Database Target -----------------> " $DB
                                trytond-admin -v -c ~/.trytond/trytond.conf -d $DB -u ${module}
                        done
}

souhome

sudo systemctl stop strytond

sudo systemctl start strytond

cd ..
cd ..

