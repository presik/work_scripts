#!/bin/sh

# As root
apt update
apt upgrade
apt install -y build-essential postgresql git python3-pip python3-dev python3-venv python-setuptools postgresql-server-dev-16 libffi-dev libpq-dev python3-simplejson
apt autoremove
locale-gen en_US en_US.UTF-8


useradd -m psk
passwd psk
usermod -aG sudo,adm psk
sudo chsh -s /bin/bash psk


# open TRYTON SERVER Port
sudo ufw allow 8000/tcp
sudo ufw allow 1157/tcp


sudo reboot

