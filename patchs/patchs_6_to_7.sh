#!/bin/sh

# Patch for fix files in trytond core


cp stock_es_v7.po    /home/psk/.virtualenvs/tryton70/lib/python3.12/site-packages/trytond/modules/stock/locale/es.po

cp sale_es_v7.po    /home/psk/.virtualenvs/tryton70/lib/python3.12/site-packages/trytond/modules/sale/locale/es.po

cp purchase_es_v7.po /home/psk/.virtualenvs/tryton70/lib/python3.12/site-packages/trytond/modules/purchase/locale/es.po

