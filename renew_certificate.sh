#!/bin/bash

# Dominio del certificado
DOMINIO="cloudx1.presik.com"

# Ruta del archivo de registro de certbot
LOG="/var/log/certbot.log"

# Fecha actual en formato Unix
HOY=$(date +%s)

# Fecha de vencimiento del certificado actual en formato Unix
VENCIMIENTO=$(openssl x509 -noout -enddate -in /etc/letsencrypt/live/$DOMINIO/fullchain.pem | awk -F= '{print $2}' | date +%s -f -)

# Umbral de renovación: 10 días
UMBRAL=$(expr 10 \* 24 \* 3600)

# Verificar si el certificado está por vencer en los próximos 10 días
if [ $(expr $VENCIMIENTO - $HOY) -lt $UMBRAL ]; then
  # Renovar el certificado
  certbot renew --quiet --post-hook "systemctl reload nginx" >> $LOG
  chmod 777 -R /etc/letsencrypt/archive/ 

fi