#!/bin/sh

#---------------------------------------------------------
#    Script Install Tryton for Home Intance
# --------------------------------------------------------

# Main functions/variables declaration

# Colors constants
NONE="$(tput sgr0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="\n$(tput setaf 3)"
BLUE="\n$(tput setaf 4)"

message () {
    # $1 : Message
    # $2 : Color
    # return : Message colorized
    local NOW="[$(date +%H:%M:%S)] "

    echo "${2}${NOW}${1}${NONE}"
}


echo '----------------------------------------------------------'
#read -p 'Introduce the tryton name user: ' nameuser
version='5.0'
max_version='5.1'
major_version_client='5.0.7'
integer_version='50'
venv='tryton'${integer_version}
nameuser=$SUDO_USER
file_bashrc=$HOME'/.bashrc'


PYT_CMD=$HOME/.virtualenvs/${venv}/bin/python3

# Updating server with POS modules
message "[INFO] Setting initializing trytond server... " ${BLUE}
DAEMON=$HOME/.virtualenvs/${venv}/bin/trytond
SETTING='-c $HOME/.trytond/trytond.conf'
DB=$1

message "[INFO] Updating server with POS modules... " ${BLUE}
systemctl stop strytond.service

POS_MODULES="
    sale_discount
    sale_product_onebarcode
    sale_goal
    sale_salesman
    sale_shop
    sale_pos
    sale_pos_frontend
"

for i in ${POS_MODULES}
    do
      su ${nameuser} -c ${DAEMON} ${SETTING} -d ${DB} -u ${i}
    done
message "[INFO] Done. " ${YELLOW}

message "[INFO] Restarting trytond server... " ${BLUE}

systemctl start strytond.service
systemctl status strytond.service
message "[INFO] Ok." ${YELLOW}
