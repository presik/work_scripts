# !bash

devs=$(ls /dev/usb/)
printer_act=""
for dev in ${devs}
do
	if [ "${dev}" == "lp0" ] || [ "${dev}" == "lp1" ]
        then
            printer_act=${dev}
	fi
done

if [ $printer_act != "" ]
        then
            socat -u TCP4-LISTEN:4242,reuseaddr,fork OPEN:/dev/usb/${printer_act}
fi
pid_active=exec lsof -ti:4242
echo "service active with pid ${pid_active}"